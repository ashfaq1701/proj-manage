<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
	protected $table   = 'comments';
	protected $guarded = ['id'];
	
	public function user()
	{
		return $this->belongsTo('App\User', 'user_id');
	}
	
	public function message()
	{
		return $this->belongsTo('App\Models\Message', 'message_id');
	}
	
	public function milestone()
	{
		return $this->belongsTo('App\Models\Milestone', 'milestone_id');
	}
	
	public function todoList()
	{
		return $this->belongsTo('App\Models\TodoList', 'todo_list_id');
	}
	
	public function file()
	{
		return $this->belongsTo('App\Models\File', 'file_id');
	}
	
	public function attachmentFiles()
	{
		return $this->hasMany('App\Models\File', 'comment_id', 'id');
	}
}