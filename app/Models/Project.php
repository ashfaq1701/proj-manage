<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
	protected $table   = 'projects';
	protected $guarded = ['id'];
	
	public function users()
	{
		return $this->belongsToMany('App\User', 'projects_users', 'project_id', 'user_id');
	}
	
	public function messages()
	{
		return $this->hasMany('App\Models\Message', 'project_id', 'id');
	}
	
	public function files()
	{
		return $this->hasMany('App\Models\File', 'project_id', 'id');
	}
	
	public function todoLists()
	{
		return $this->hasMany('App\Models\TodoList', 'project_id', 'id');
	}
	
	public function milestones()
	{
		return $this->hasMany('App\Models\Milestone', 'project_id', 'id');
	}
}