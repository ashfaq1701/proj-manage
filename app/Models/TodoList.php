<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TodoList extends Model
{
	protected $table   = 'todo_lists';
	protected $guarded = ['id'];
	
	public function user()
	{
		return $this->belongsTo('App\User', 'user_id');
	}
	
	public function todoListStatuses()
	{
		return $this->hasMany('App\Models\TodoListStatus', 'todo_list_id', 'id');
	}
	
	public function comments()
	{
		return $this->hasMany('App\Models\Comment', 'todo_list_id', 'id');
	}
	
	public function milestone()
	{
		return $this->belongsTo('App\Models\Milestone', 'milestone_id');
	}
	
	public function project()
	{
		return $this->belongsTo('App\Models\Project', 'project_id');
	}
}