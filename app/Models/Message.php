<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
	protected $table   = 'messages';
	protected $guarded = ['id'];
	
	public function user()
	{
		return $this->belongsTo('App\User', 'user_id');
	}
	
	public function project()
	{
		return $this->belongsTo('App\Models\Project', 'project_id');
	}
	
	public function category()
	{
		return $this->belongsTo('App\Models\Category', 'category_id');
	}
	
	public function comments()
	{
		return $this->hasMany('App\Models\Comment', 'message_id', 'id');
	}
	
	public function messageListeners()
	{
		return $this->belongsToMany('App\User', 'messages_users', 'message_id', 'user_id');
	}
	
	public function attachmentFiles()
	{
		return $this->hasMany('App\Models\File', 'message_id', 'id');
	}
}