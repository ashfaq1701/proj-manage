<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TodoListStatus extends Model
{
	protected $table   = 'todo_list_statuses';
	protected $guarded = ['id'];
	
	public function todoList()
	{
		return $this->belongsTo('App\Model\TodoList', 'todo_list_id');
	}
}