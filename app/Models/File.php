<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
	protected $table   = 'files';
	protected $guarded = ['id'];
	
	public function user()
	{
		return $this->belongsTo('App\User', 'user_id');
	}
	
	public function project()
	{
		return $this->belongsTo('App\Models\Project', 'project_id');
	}
	
	public function comments()
	{
		return $this->hasMany('App\Models\Comment', 'file_id', 'id');
	}
	
	public function attachmentToMessage()
	{
		return $this->belongsTo('App\Models\Message', 'message_id');
	}
	
	public function attachmentToComment()
	{
		return $this->belongsTo('App\Models\Comment', 'comment_id');
	}
	
	public function fileListeners()
	{
		return $this->belongsToMany('App\User', 'files_users', 'file_id', 'user_id');
	}
}