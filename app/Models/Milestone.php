<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Milestone extends Model
{
	protected $table   = 'milestones';
	protected $guarded = ['id'];
	
	public function user()
	{
		return $this->belongsTo('App\User', 'user_id');
	}
	
	public function comments()
	{
		return $this->hasMany('App\Models\Comment', 'milestone_id', 'id');
	}
	
	public function todoLists()
	{
		return $this->hasMany('App\Models\TodoList', 'milestone_id', 'id');
	}
	
	public function project()
	{
		return $this->belongsTo('App\Models\Project', 'project_id');
	}
}