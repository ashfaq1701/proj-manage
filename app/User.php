<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function ims()
    {
    	return $this->hasMany('App\Models\Im', 'user_id', 'id');
    }
    
    public function files()
    {
    	return $this->hasMany('App\Models\File', 'user_id', 'id');
    }
    
    public function comments()
    {
    	return $this->hasMany('App\Models\Comment', 'user_id', 'id');
    }
    
    public function messages()
    {
    	return $this->hasMany('App\Models\Message', 'user_id', 'id');
    }
    
    public function milestones()
    {
    	return $this->hasMany('App\Models\Milestone'. 'user_id', 'id');
    }
    
    public function todoLists()
    {
    	return $this->hasMany('App\Models\TodoList', 'user_id', 'id');
    }
    
    public function projects()
    {
    	return $this->belongsToMany('App\Models\Project', 'projects_users', 'user_id', 'project_id');
    }
    
    public function listenerToMessages()
    {
    	return $this->belongsToMany('App\Models\Message', 'messages_users', 'user_id', 'message_id');
    }
    
    public function listenerToFiles()
    {
    	return $this->belongsToMany('App\Models\File', 'files_users', 'user_id', 'file_id');
    }
    
}
