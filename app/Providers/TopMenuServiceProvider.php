<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use View;

class TopMenuServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('layouts.app-login', 'App\Http\Composers\TopMenuComposer');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
