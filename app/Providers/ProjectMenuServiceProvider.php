<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use View;

class ProjectMenuServiceProvider extends ServiceProvider
{
	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		View::composer('layouts.app-login-project', 'App\Http\Composers\ProjectMenuComposer');
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}
}
