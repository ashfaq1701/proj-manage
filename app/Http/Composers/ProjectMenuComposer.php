<?php

namespace App\Http\Composers;

use Illuminate\Contracts\View\View;

class ProjectMenuComposer
{
	protected $menu;

	public function __construct()
	{
		
	}

	public function compose(View $view)
	{
		$data = $view->getData();
		$project = $data['project'];
		$this->menu = [
				'Messages' => ['/'.$project->id.'/messages' => ['admin', 'client', 'dev']],
				'To-Dos' => ['/'.$project->id.'/todos' => ['admin', 'client', 'dev']],
				'Milestones' => ['/'.$project->id.'/milestones' => ['admin', 'client', 'dev']],
				'Files' => ['/'.$project->id.'/files' => ['admin', 'client', 'dev']]
		];
		$view->with('menu', $this->menu);
	}
}