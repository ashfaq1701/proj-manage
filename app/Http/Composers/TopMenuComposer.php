<?php

namespace App\Http\Composers;

use Illuminate\Contracts\View\View;

class TopMenuComposer
{
    protected $menu;

    public function __construct()
    {
        $this->menu = [
            'Dashboard' => ['/home' => ['admin', 'client', 'dev']],
        	'Users' => ['/users' => ['admin']],
        	'Categories' => ['/categories' => ['admin']],
        	'Projects' => ['/projects' => ['admin', 'client', 'dev']]
        ];
    }

    public function compose(View $view)
    {
        $view->with('menu', $this->menu);
    }
}