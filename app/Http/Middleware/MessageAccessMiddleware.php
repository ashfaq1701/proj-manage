<?php

namespace App\Http\Middleware;

use Closure;

class MessageAccessMiddleware
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		$messageId = $request->message_id;
		$user = $request->user();
		$messages = $user->messages()->get();
		if(($user->type=='admin') || ($messages->contains('id', $messageId)))
		{
			return $next($request);
		}
		else
		{
			return response()->view('errors.error-login-project', ['error'=>"You don't have access to this message"], 401);
		}
	}
}
