<?php

namespace App\Http\Middleware;

use Closure;

class FileAccessMiddleware
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		$fileId = $request->file_id;
		$user = $request->user();
		$files = $user->files()->get();
		if(($user->type=='admin') || ($files->contains('id', $fileId)))
		{
			return $next($request);
		}
		else
		{
			return response()->view('errors.error-login-project', ['error'=>"You don't have access to this file"], 401);
		}
	}
}
