<?php

namespace App\Http\Middleware;

use Closure;

class ProjectAccessMiddleware
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		$projectId = $request->project_id;
		$user = $request->user();
		$projects = $user->projects()->get();
		if(($user->type=='admin') || ($projects->contains('id', $projectId)))
		{
			return $next($request);
		}
		else
		{
			return response()->view('errors.error-login-noproject', ['error'=>"You don't have access to this project"], 401);
		}
	}
}
