<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
	if(Auth::check())
	{
		return redirect('/home');
	}
	return redirect('/login');
});

// Authentication Routes...
$this->get('login', 'Auth\AuthController@showLoginForm');
$this->post('login', 'Auth\AuthController@login');
$this->get('logout', 'Auth\AuthController@logout');

// Registration Routes...
$this->get('register', 'Auth\AuthController@registrationForm');
$this->post('register', 'Auth\AuthController@register');

// Password Reset Routes...
$this->get('password/reset/{token?}', 'Auth\PasswordController@showResetForm');
$this->post('password/email', 'Auth\PasswordController@sendResetLinkEmail');
$this->post('password/reset', 'Auth\PasswordController@reset');

Route::group(['middleware' => 'auth'], function () {
	Route::get('/home', 'HomeController@index');
	Route::get('/users/companies', 'Resource\UserController@companies');
	
	Route::group(['middleware' => 'role:admin'], function () {
		Route::get('/users', 'Resource\UserController@index');
		Route::get('/users/invite', 'Resource\UserController@create');
		Route::post('/users/invite', 'Resource\UserController@store');
		Route::get('/users/edit/{id}', 'Resource\UserController@edit');
		Route::post('/users/edit/{id}', 'Resource\UserController@update');
		Route::get('/users/delete/{id}', 'Resource\UserController@destroy');
		
		Route::get('/categories', 'Resource\CategoryController@index');
		Route::post('/categories/create', 'Resource\CategoryController@store');
		Route::get('/categories/edit/{id}', 'Resource\CategoryController@edit');
		Route::post('/categories/edit/{id}', 'Resource\CategoryController@update');
		Route::get('/categories/delete/{id}', 'Resource\CategoryController@destroy');
		
		Route::get('/projects', 'Resource\ProjectController@index');
		Route::get('/projects/create', 'Resource\ProjectController@create');
		Route::post('/projects/create', 'Resource\ProjectController@store');
		Route::get('/projects/edit/{id}', 'Resource\ProjectController@edit');
		Route::post('/projects/edit/{id}', 'Resource\ProjectController@update');
		Route::get('/projects/delete/{id}', 'Resource\ProjectController@destroy');
	});
	
	Route::group(['middleware' => 'project-access'], function() {
		Route::get('/{project_id}/messages', 'Resource\MessageController@index');
		Route::get('/{project_id}/messages/create', 'Resource\MessageController@create');
		Route::post('/{project_id}/messages/create', 'Resource\MessageController@store');
		Route::get('/{project_id}/messages/edit/{message_id}', 'Resource\MessageController@edit');
		Route::post('/{project_id}/messages/edit/{message_id}', 'Resource\MessageController@update');
		Route::get('/{project_id}/messages/{message_id}', 'Resource\MessageController@show');
		
		Route::get('/{project_id}/todos', 'Resource\TodoListController@index');
		Route::get('/{project_id}/todos/create', 'Resource\TodoListController@create');
		Route::post('/{project_id}/todos/create', 'Resource\TodoListController@store');
		Route::get('/{project_id}/todos/edit/{todo_id}', 'Resource\TodoListController@edit');
		Route::post('/{project_id}/todos/edit/{todo_id}', 'Resource\TodoListController@update');
		Route::get('/{project_id}/todos/{todo_id}', 'Resource\TodoListController@show');
		
		Route::get('/{project_id}/milestones', 'Resource\MilestoneController@index');
		Route::get('/{project_id}/milestones/create', 'Resource\MilestoneController@create');
		Route::post('/{project_id}/milestones/create', 'Resource\MilestoneController@store');
		Route::get('/{project_id}/milestones/edit/{milestone_id}', 'Resource\MilestoneController@edit');
		Route::post('/{project_id}/milestones/edit/{milestone_id}', 'Resource\MilestoneController@update');
		Route::get('/{project_id}/milestones/{milestone_id}', 'Resource\MilestoneController@show');
		
		Route::get('/{project_id}/files', 'Resource\FileController@index');
		Route::get('/{project_id}/files/create', 'Resource\FileController@create');
		Route::post('/{project_id}/files/create', 'Resource\FileController@store');
		Route::get('/{project_id}/files/edit/{file_id}', 'Resource\FileController@edit');
		Route::post('/{project_id}/files/edit/{file_id}', 'Resource\FileController@update');
		Route::get('/{project_id}/files/{file_id}', 'Resource\FileController@show');
	});
});


