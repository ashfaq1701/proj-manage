<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use Hash;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        	'company' => 'required',
        	'office_no' => 'required'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(Request $request)
    {	
    	//
    }
    
    protected function register(Request $request)
    {
    	while (true)
    	{
    		$message = 'Error in registration.';
    		$data = $request->all();
    		if(empty($data['id']) || empty($data['reg_token']))
    		{
    			break;
    		}
    		$user = User::find($data['id']);
    		if(empty($user))
    		{
    			break;
    		}
    		if(empty($user->reg_token))
    		{
    			break;
    		}
    		if($user->reg_token != $data['reg_token'])
    		{
    			break;
    		}
    		if($data['password'] != $data['repeat_password'])
    		{
    			$message = 'Passwords not same.';
    			break;
    		}
    		$user->name = $data['name'];
    		$user->password = Hash::make($data['password']);
    		$user->company = $data['company'];
    		$user->position = $data['position'];
    		$user->office_no = $data['office_no'];
    		$user->ext = $data['ext'];
    		$user->mobile_no = $data['mobile_no'];
    		$user->fax_no = $data['fax_no'];
    		$user->home_no = $data['home_no'];
    		$user->reg_token = null;
    		$user->save();
    		return response()->view('auth.login', ['alert_type'=>'success', 'message'=>'Registration successful']);
    	}
    	return response()->view('errors.error-nologin', ['error'=>$message], 401);
    }
    
    protected function registrationForm(Request $request)
    {
    	while (true)
    	{
    		$data = $request->all();
    		if(empty($data['id']) || empty($data['token']))
    		{
    			break;
    		}
    		$user = User::find($data['id']);
    		if(empty($user))
    		{
    			break;
    		}
    		if(empty($user->reg_token))
    		{
    			break;
    		}
    		if($user->reg_token != $data['token'])
    		{
    			break;
    		}
    		return response()->view('auth.register', ['user'=>$user]);
    	}
    	return response()->view('errors.error-nologin', ['error'=>"You don't have access to this page"], 401);
    }
    
}
