<?php

namespace App\Http\Controllers\Resource;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Hash;
use DB;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	return view('users.index', ['users'=>User::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.invite');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	$names = $request->input('name');
    	$emails = $request->input('email');
    	$positions = $request->input('position');
    	$companies = $request->input('organization');
    	$types = $request->input('type');
    	for($i = 0; $i < count($names); $i++)
    	{
    		$name = $names[$i];
    		$email = $emails[$i];
    		$position = $positions[$i];
    		$company = $companies[$i];
    		$type = $types[$i];
    		if(empty($name) && empty($email))
    		{
    			continue;
    		}
    		if(empty($name) || empty($email))
    		{
    			return response()->view('errors.error-nologin', ['error'=>"Data not sent properly"], 401);
    		}
    		$user = new User();
    		$user->name = $name;
    		$user->email = $email;
    		$user->password = Hash::make(str_random(10));
    		$user->position = $position;
    		$user->company = $company;
    		$user->type = $type;
    		$user->reg_token = str_random(40);
    		$user->save();
    	}
    	return view('users.index', ['users'=>User::all()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('users.edit', ['user'=>$user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    	$data = $request->all();
        $user = User::find($id);
        $user->email = $data['email'];
        $user->name = $data['name'];
        $user->company = $data['company'];
        $user->position = $data['position'];
        $user->office_no = $data['office_no'];
        $user->ext = $data['ext'];
        $user->mobile_no = $data['mobile_no'];
        $user->fax_no = $data['fax_no'];
        $user->home_no = $data['home_no'];
        $user->save();
        return view('users.edit', ['user'=>$user, 'alert_type'=>'success', 'message'=>'User updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
    }
    
    public function companies()
    {
    	$companies = DB::table('users')
    		->select('company')
    		->groupBy('company')
    		->get();
    	$arr = [];
    	foreach ($companies as $company)
    	{
    		foreach ($company as $key=>$value)
    		{
    			$arr[] = array('label'=>$value, 'value'=>$value);
    		}
    	}
    	return response()->json($arr);
    	
    }
}
