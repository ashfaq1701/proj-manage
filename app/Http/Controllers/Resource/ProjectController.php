<?php

namespace App\Http\Controllers\Resource;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Project;
use Auth;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	if(Auth::user()->type == 'admin')
    	{
    		$projects = Project::all();
    	}
    	else
    	{
    		$projects = Auth::user()->projects();
    	}
    	return view('projects.index', ['projects'=>$projects]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('projects.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $name = $data['name'];
        $description = $data['description'];
        $project = new Project();
        $project->name = $name;
        $project->description = $description;
        $project->status = 'open';
        $project->save();
        return view('projects.index', ['projects'=>Project::all(), 'alert_type'=>'success', 'message'=>'Project created successfully.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	$project = Project::find($id);
    	return view('projects.edit', ['project'=>$project]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    	$data = $request->all();
    	$project = Project::find($id);
    	$project->name = $data['name'];
    	$project->description = $data['description'];
    	$project->save();
    	return view('projects.edit', ['project'=>$project, 'alert_type'=>'success', 'message'=>'Project updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    	$project = Project::find($id);
    	$project->delete();
    }
}
