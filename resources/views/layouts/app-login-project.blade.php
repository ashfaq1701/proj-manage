@extends('layouts.app-login')

@section('fullcontent-login')
	<!-- Sidebar -->
	@include('layouts/sidebar')
	<div class="col col_10_of_12">
		@yield('content')
	</div>
@endsection
