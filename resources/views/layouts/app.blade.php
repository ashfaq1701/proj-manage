<!DOCTYPE html>
<html>
    <head>
        <title>TrendyBlog | Premium Magazine Template</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        
        <!-- Favicons -->
        <link rel="icon" href="favicon.png">
        
        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/all.css') }}">
        
        <!-- Responsive -->
        <link rel="stylesheet" type="text/css" media="(max-width:768px)" href="{{ asset('css/responsive-0.css') }}">
        <link rel="stylesheet" type="text/css" media="(min-width:769px) and (max-width:992px)" href="{{ asset('css/responsive-768.css') }}">
        <link rel="stylesheet" type="text/css" media="(min-width:993px) and (max-width:1200px)" href="{{ asset('css/responsive-992.css') }}">
        <link rel="stylesheet" type="text/css" media="(min-width:1201px)" href="{{ asset('css/responsive-1200.css') }}">
        <link href='http://fonts.googleapis.com/css?family=Titillium+Web:300,300italic,400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <!-- Wrapper -->
        <div id="wrapper" class="wide">            
            <!-- Header -->
            <header id="header" role="banner">                
            	<!-- Header meta -->
            	@include('layouts/header-meta')
            	<!-- Header main -->
            	<div id="header_main" class="sticky header_main">
					<div class="container">
    					<!-- Logo -->
       					<div class="site_brand">
        					<h1 id="site_title"><a href="index.html">Brac<span>Implementation</span></a></h1>
           					<h2 id="site_description">HJC Consulting</h2>
        				</div><!-- End Logo -->
        				@yield('topmenu')
					</div>
				</div><!-- End Header main -->
        	</header><!-- End Header -->
            <!-- Section -->
            <section>
                <div class="container">
                    <div class="row">
                        <!-- Sidebar -->
                        @yield('fullcontent')
                    </div>
                </div>
            </section><!-- End Section -->
            <!-- Footer -->
            @include('layouts/footer')
            <!-- Copyright -->
            @include('layouts/copyright')
        </div><!-- End Wrapper -->
        
        <!-- Scripts -->
        <script type="text/javascript" src="{{ asset('js/all.js') }}"></script>             
    </body>
</html>
