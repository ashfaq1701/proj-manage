<div class="header_meta">
	<div class="container">
    	<!-- Weather forecast -->
        <div class="weather_forecast">
        	<i class="wi wi-day-lightning"></i>
            <span class="city">Dhaka, Bangladesh</span>
            <span class="temp">18°C</span>
       	</div><!-- End Weather forecast -->
        <!-- Top menu -->
        <nav class="top_navigation" role="navigation">
        	<span class="top_navigation_toggle"><i class="fa fa-reorder"></i></span>
            <ul class="menu">
            	<li class="search_icon_form"><a href="#"><i class="fa fa-search"></i></a>
                	<div class="sub-search">
                    	<form>
                        	<input type="search" placeholder="Search...">
                            <input type="submit" value="Search">
                        </form>
                   	</div>
                </li>
            </ul>
       	</nav><!-- End Top menu -->
	</div>
</div><!-- End Header meta -->