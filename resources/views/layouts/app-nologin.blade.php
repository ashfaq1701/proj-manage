@extends('layouts.app')

@section('fullcontent')
<div class="col col_12_of_12">
	@yield('content')
</div>
@endsection