<footer id="footer">
	<div class="container">
    	<div class="row">
        	<div class="col col_3_of_12"> 
        		@yield('footer-col-1')                       
           	</div>
            <div class="col col_3_of_12">
            	@yield('footer-col-2')
            </div>
            <div class="col col_3_of_12">
            	@yield('footer-col-3')         
            </div>
            <div class="col col_3_of_12">
            	@yield('footer-col-4')            
            </div>
        </div>
    </div>
</footer><!-- End Footer -->