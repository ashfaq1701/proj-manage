@extends('layouts.app-login')

@section('fullcontent-login')
	<div class="col col_12_of_12">
		@yield('content')
	</div>
@endsection