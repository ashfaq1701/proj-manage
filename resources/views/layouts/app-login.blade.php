@extends('layouts.app')

@section('topmenu')
	<nav class="site_navigation" role="navigation">
    	<span class="site_navigation_toggle"><i class="fa fa-reorder"></i></span>
        <ul class="menu">
        	@foreach($menu as $name=>$arr)
    			@foreach($arr as $path=>$roles)
    				@if (in_array(Auth::user()->type, $roles))
    					<li class="menu-item">
    						<a href="{{$path}}">{{$name}}</a>
    					</li>
    				@endif
    			@endforeach
    		@endforeach
        </ul>
    </nav>
@endsection

@section('fullcontent')
	@yield('fullcontent-login')
@endsection