@extends('layouts.app-nologin')

@section('content')
<h2>Register</h2>
<form action="/register" method="POST">
	{!! csrf_field() !!}
	<input type="hidden" name="id" value="{{ $user->id }}">
	<input type="hidden" name="reg_token" value="{{ $user->reg_token }}">
	<p>
		<label>Email</label>
		<input type="text" name="email" value="{{ $user->email }}" disabled="disabled">
	</p>
	<p>
		<label>Name</label>
		<input type="text" name="name" value="{{ $user->name }}">
	</p>
	<p>
   		<label>Password*</label>
        <input type="password" name="password">
    </p>
    <p>
    	<label>Repeat Password*</label>
    	<input type="password" name="repeat_password">
    </p>
    <p>
   		<label>Company*</label>
        <input type="text" name="company" value="{{ $user->company }}">
    </p>
    <p>
   		<label>Position</label>
        <input type="text" name="position" value="{{ $user->position }}">
    </p>
    <p>
    	<label>Office No*</label>
    	<input type="text" name="office_no">
    </p>
    <p>
    	<label>Extension</label>
    	<input type="text" name="ext">
    </p>
    <p>
    	<label>Mobile No</label>
    	<input type="text" name="mobile_no">
    </p>
    <p>
    	<label>Fax No</label>
    	<input type="text" name="fax_no">
    </p>
    <p>
    	<label>Home No</label>
    	<input type="text" name="home_no">
    </p>
    <p>
   		<button type="submit" class="btn">Submit</button>
    </p>
</form>
@endsection
