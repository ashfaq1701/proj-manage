@extends('layouts.app-nologin')

@section('content')

@if(!empty($message))
	<div class="alert {{ $alert_type }}">
		<p>{{ $message }}</p>
	</div>
@endif

<h2>Login</h2>

<form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
	{!! csrf_field() !!}

	<p class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
    	<label>Email</label>
		<input type="text" name="email" value="{{ old('email') }}">
        @if ($errors->has('email'))
        	<span class="help-block">
            	<strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
   	</p>

   	<p class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
    	<label>Password</label>
        <input type="password" name="password">
		@if ($errors->has('password'))
        	<span class="help-block">
         		<strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
	</p>

	<p>
    	<label>
        	<input type="checkbox" name="remember"> Remember Me
        </label>
   	</p>

   	<p>
  		<button type="submit" class="btn">Login</button>
		<a class="btn btn-link" href="{{ url('/password/reset') }}">Forgot Your Password?</a>
   	</p>
</form>
                
@endsection
