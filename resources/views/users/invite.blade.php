@extends('layouts.app-login-noproject')

@section('content')
	<h2>Invite Users</h2>
	<form action="/users/invite" method="POST">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<table>
		<thead>
			<tr>
				<th>Name</th>
				<th>Email</th>
				<th>Job Title</th>
				<th>Organization</th>
				<th>Type</th>
			</tr>
		</thead>
		<tbody>
			@for($i = 0; $i < 4; $i++)
				<tr>
					<td>
						<input type="text" name="name[]" class="name">
					</td>
					<td>
						<input type="text" name="email[]" class="email">
					</td>
					<td>
						<input type="text" name="position[]" class="position">
					</td>
					<td>
						<input name="organization[]" class="organization company-autocomplete">
					</td>
					<td>
						<select name="type[]" class="type">
							<option value="client">Client</option>
							<option value="dev">Developer</option>
						</select>
					</td>
				</tr>
			@endfor
			</tr>
		</tbody>
		</table>
		<p>
			<button type="submit" class="btn">Submit</button>
		</p>
	</form>
@endsection