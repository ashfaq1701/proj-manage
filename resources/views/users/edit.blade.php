@extends('layouts.app-login-noproject')

@section('content')

@if(!empty($message))
	<div class="alert {{ $alert_type }}">
		<p>{{ $message }}</p>
	</div>
@endif

<h2>Edit User</h2>
<form action="/users/edit/{{ $user->id }}" method="POST">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<p>
		<label>Email</label>
		<input type="text" name="email" value="{{ $user->email }}">
	</p>
	<p>
		<label>Name</label>
		<input type="text" name="name" value="{{ $user->name }}">
	</p>
    <p>
   		<label>Company*</label>
        <input type="text" name="company" value="{{ $user->company }}">
    </p>
    <p>
   		<label>Position*</label>
        <input type="text" name="position" value="{{ $user->position }}">
    </p>
    <p>
    	<label>Office No*</label>
    	<input type="text" name="office_no" value="{{ $user->office_no }}">
    </p>
    <p>
    	<label>Extension</label>
    	<input type="text" name="ext" value="{{ $user->ext }}">
    </p>
    <p>
    	<label>Mobile No</label>
    	<input type="text" name="mobile_no" value="{{ $user->mobile_no }}">
    </p>
    <p>
    	<label>Fax No</label>
    	<input type="text" name="fax_no" value="{{ $user->fax_no }}">
    </p>
    <p>
    	<label>Home No</label>
    	<input type="text" name="home_no" value="{{ $user->home_no }}">
    </p>
    <p>
   		<button type="submit" class="btn">Submit</button>
    </p>
</form>
@endsection