@extends('layouts.app-login-noproject')

@section('content')
<p>
	<a href="/users/invite">Invite New User</a>
</p>

<h2>All Users</h2>
<table class="datatable">
	<thead>
		<tr>
			<th>Name</th>
			<th>Email</th>
			<th>Company</th>
			<th>Position</th>
			<th>Type</th>
			<th>Status</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		@foreach($users as $user)
			<tr>
				<td>{{ empty($user->name) ? '-' : $user->name }}</td>
				<td>{{ $user->email }}</td>
				<td>{{ isset($user->company) ? $user->company : '-' }}</td>
				<td>{{ isset($user->position) ? $user->position : '-' }} </td>
				<td>{{ $user->type }}</td>
				<td>{{ empty($user->reg_token) ? 'Active' : 'Invited' }}</td>
				<td>
					<input type="hidden" id="resource" value="users">
					<a href="/users/edit/{{ $user->id }}"><i class="fa fa-pencil-square-o edit" aria-hidden="true"></i></a>
					<a id="data{{$user->id}}" class="remove confirm"><i class="fa fa-minus-circle" aria-hidden="true"></i></a>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>
@endsection