@extends('layouts.app-login-project')

@section('content')
	@if(!empty($message))
		<div class="alert {{ $alert_type }}">
			<p>{{ $message }}</p>
		</div>
	@endif
	<p>
		<a href="/projects/create">Create New Project</a>
	</p>
@endsection