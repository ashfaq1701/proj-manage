@extends('layouts.app-login-noproject')

@section('content')
	@if(!empty($message))
		<div class="alert {{ $alert_type }}">
			<p>{{ $message }}</p>
		</div>
	@endif
	
	<h2>Create Category</h2>
	<form action="/categories/create" method="POST">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<p>
			<label>Category</label>
			<input type="text" name="category">
		</p>
		<p>
   			<button type="submit" class="btn">Submit</button>
    	</p>
	</form>
	
	<h2>All Categories</h2>
	<table class="datatable">
		<thead>
			<tr>
				<th>Category</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			@foreach($categories as $category)
				<tr>
					<td>{{ $category->category }}</td>
					<td>
						<input type="hidden" id="resource" value="categories">
						<a href="/categories/edit/{{ $category->id }}"><i class="fa fa-pencil-square-o edit" aria-hidden="true"></i></a>
						<a id="data{{$category->id}}" class="remove confirm"><i class="fa fa-minus-circle" aria-hidden="true"></i></a>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
	
@endsection