@extends('layouts.app-login-noproject')

@section('content')

	@if(!empty($message))
		<div class="alert {{ $alert_type }}">
			<p>{{ $message }}</p>
		</div>
	@endif
	
	<h2>Edit Category</h2>
	<form action="/categories/edit/{{ $category->id }}" method="POST">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<p>
			<label>Category</label>
			<input type="text" name="category" value="{{ $category->category }}">
		</p>
		<p>
   			<button type="submit" class="btn">Submit</button>
    	</p>
	</form>
	
@endsection