@extends('layouts.app-login-noproject')

@section('content')

	@if(!empty($message))
		<div class="alert {{ $alert_type }}">
			<p>{{ $message }}</p>
		</div>
	@endif
	
	<h2>Edit Project</h2>
	<form action="/projects/edit/{{ $project->id }}" method="POST">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<p>
			<label>Name</label>
			<input type="text" name="name" value="{{ $project->name }}">
		</p>
		<p>
			<label>Description</label>
			<textarea class="editor" name="description">{{ $project->description }}</textarea>
		</p>
		<p>
   			<button type="submit" class="btn">Submit</button>
    	</p>
	</form>
@endsection

