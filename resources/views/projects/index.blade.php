@extends('layouts.app-login-noproject')

@section('content')
	@if(!empty($message))
		<div class="alert {{ $alert_type }}">
			<p>{{ $message }}</p>
		</div>
	@endif
	@if (Auth::user()->type == 'admin')
		<p>
			<a href="/projects/create">Create New Project</a>
		</p>
	@endif
	
	<h2>All Projects</h2>
	<table class="datatable">
		<thead>
			<tr>
				<th>Name</th>
				<th>Status</th>
				<th>Start Date</th>
				<th>End Date</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			@foreach($projects as $project)
				<tr>
					<td><a href="/{{ $project->id }}/messages">{{ empty($project->name) ? '-' : $project->name }}</a></td>
					<td>{{ empty($project->status) ? '-' : $project->status }}</td>
					<td>{{ $project->created_at }}</td>
					<td>{{ empty($project->end_date) ? '-' : $project->end_date }}</td>
					<td>
						<input type="hidden" id="resource" value="projects">
						<a href="/projects/edit/{{ $project->id }}"><i class="fa fa-pencil-square-o edit" aria-hidden="true"></i></a>
						<a id="data{{$project->id}}" class="remove confirm"><i class="fa fa-minus-circle" aria-hidden="true"></i></a>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
@endsection