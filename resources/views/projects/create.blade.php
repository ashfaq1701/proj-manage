@extends('layouts.app-login-noproject')

@section('content')

	<h2>Create Project</h2>
	<form action="/projects/create" method="POST">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<p>
			<label>Name</label>
			<input type="text" name="name">
		</p>
		<p>
			<label>Description</label>
			<textarea class="editor" name="description"></textarea>
		</p>
		<p>
   			<button type="submit" class="btn">Submit</button>
    	</p>
	</form>
@endsection