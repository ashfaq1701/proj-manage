$(document).ready(function() {
    $('.datatable').dataTable();
    $("a.remove").easyconfirm();
    $('a.remove').click(function()
    {
    	var resource = $('#resource').val();
    	var id = $(this).attr('id').replace('data', '');
    	var link = $(this);
        $.ajax({
        	url: "/"+resource+"/delete/"+id
        })
        .done(function( data ) {
        	link.parent().parent().remove();
        });            
    });
    $(".editor").trumbowyg();
    
    $('.company-autocomplete').autocomplete({
        source: "/users/companies",
        minLength: 2,
    });
});