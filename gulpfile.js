var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.sass('app.scss');

    mix.styles([
                'colors.css',
                'fontawesome.css',
                'normalize.css',
                'style.css',
                'typography.css',
                'weather.css',
                'jquery-ui.css',
                'datatables.min.css',
                'trumbowyg.min.css'
            ]);
    mix.styles([
                'responsive-0.css'
            ], 'public/css/responsive-0.css');
    mix.styles([
                'responsive-768.css'
            ], 'public/css/responsive-768.css');
    mix.styles([
                'responsive-992.css'
            ], 'public/css/responsive-992.css');
    mix.styles([
                'responsive-1200.css'
            ], 'public/css/responsive-1200.css');
    mix.scripts([
                 'jqueryscript.min.js',
                 'jqueryuiscript.min.js',
                 'datatables.min.js',
                 'jquery.easy-confirm-dialog.min.js',
                 'bxslider.min.js',
                 'easing.min.js',
                 'fitvids.min.js',
                 'init.js',
                 'magnific.min.js',
                 'shiv.min.js',
                 'smoothscroll.min.js',
                 'viewportchecker.min.js',
                 'trumbowyg.min.js',
                 'custom.js'
             ]);
    
});
