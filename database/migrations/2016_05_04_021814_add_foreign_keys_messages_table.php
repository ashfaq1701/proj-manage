<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::table('messages', function(Blueprint $table) {
    		$table->foreign('user_id')->references('id')->on('users')->onDelete('restrict')->onUpdate('cascade');
    		$table->foreign('project_id')->references('id')->on('projects')->onDelete('restrict')->onUpdate('cascade');
    		$table->foreign('category_id')->references('id')->on('categories')->onDelete('restrict')->onUpdate('cascade');
    	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::table('messages', function(Blueprint $table) {
    		 $table->dropForeign('user_id');
    		 $table->dropForeign('project_id');
    		 $table->dropForeign('category_id');
    	});
    }
}
