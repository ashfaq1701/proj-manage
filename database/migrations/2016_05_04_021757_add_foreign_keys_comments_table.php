<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::table('comments', function(Blueprint $table) {
    		$table->foreign('user_id')->references('id')->on('users')->onDelete('restrict')->onUpdate('cascade');
    		$table->foreign('message_id')->references('id')->on('messages')->onDelete('restrict')->onUpdate('cascade');
    		$table->foreign('milestone_id')->references('id')->on('milestones')->onDelete('restrict')->onUpdate('cascade');
    		$table->foreign('todo_list_id')->references('id')->on('todo_lists')->onDelete('restrict')->onUpdate('cascade');
    		$table->foreign('file_id')->references('id')->on('files')->onDelete('restrict')->onUpdate('cascade');
    	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::table('comments', function(Blueprint $table) {
    		 $table->dropForeign('user_id');
    		 $table->dropForeign('message_id');
    		 $table->dropForeign('milestone_id');
    		 $table->dropForeign('todo_list_id');
    		 $table->dropForeign('file_id');
    	});
    }
}
