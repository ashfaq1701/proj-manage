<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::create('ims', function (Blueprint $table) {
    		$table->increments('id');
    		$table->string('username', 50);
    		$table->string('type', 50);
    		$table->integer('user_id')->unsigned()->nullable();
    		$table->nullableTimestamps();
    	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ims');
    }
}
