<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysToProjectsUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::table('projects_users', function(Blueprint $table) {
    		$table->foreign('project_id')->references('id')->on('projects')->onDelete('restrict')->onUpdate('cascade');
    		$table->foreign('user_id')->references('id')->on('users')->onDelete('restrict')->onUpdate('cascade');
    	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::table('projects_users', function(Blueprint $table) {
    		 $table->dropForeign('project_id');
    		 $table->dropForeign('user_id');
    	});
    }
}
