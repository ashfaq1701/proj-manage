<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysToFilesUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::table('files_users', function(Blueprint $table) {
    		$table->foreign('file_id')->references('id')->on('files')->onDelete('restrict')->onUpdate('cascade');
    		$table->foreign('user_id')->references('id')->on('users')->onDelete('restrict')->onUpdate('cascade');
    	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::table('files_users', function(Blueprint $table) {
    		 $table->dropForeign('file_id');
    		 $table->dropForeign('user_id');
    	});
    }
}
