<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysImsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::table('ims', function(Blueprint $table) {
    		$table->foreign('user_id')->references('id')->on('users')->onDelete('restrict')->onUpdate('cascade');
    	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ims', function(Blueprint $table)
        {
        	$table->dropForeign('user_id');
        });
    }
}
