<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->string('company', 100)->nullable();
            $table->string('office_no', 20)->nullable();
            $table->string('ext', 10)->nullable();
            $table->string('mobile_no', 20)->nullable();
            $table->string('fax_no', 20)->nullable();
            $table->string('home_no', 20)->nullable();
            $table->string('type', 10)->nullable();
            $table->string('reg_token', 50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
