<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::create('messages', function (Blueprint $table) {
    		$table->increments('id');
    		$table->text('message');
    		$table->tinyInteger('is_private')->nullable();
    		$table->integer('user_id')->unsigned()->nullable();
    		$table->integer('project_id')->unsigned()->nullable();
    		$table->integer('category_id')->unsigned()->nullable();
    		$table->nullableTimestamps();
    	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('messages');
    }
}
