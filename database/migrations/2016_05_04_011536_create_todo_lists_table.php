<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTodoListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::create('todo_lists', function (Blueprint $table) {
    		$table->increments('id');
    		$table->string('name', 200);
    		$table->tinyInteger('is_private')->nullable();
    		$table->integer('milestone_id')->unsigned()->nullable();
    		$table->integer('project_id')->unsigned()->nullable();
    		$table->integer('user_id')->unsigned()->nullable();
    		$table->nullableTimestamps();
    	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('todo_lists');
    }
}
