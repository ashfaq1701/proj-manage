<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTodoListStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::create('todo_list_statuses', function (Blueprint $table) {
    		$table->increments('id');
    		$table->integer('todo_list_id')->unsigned()->nullable();
    		$table->tinyInteger('status');
    		$table->nullableTimestamps();
    	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('todo_list_statuses');
    }
}
