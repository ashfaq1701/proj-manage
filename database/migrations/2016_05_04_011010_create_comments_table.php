<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::create('comments', function (Blueprint $table) {
    		$table->increments('id');
    		$table->text('comment');
    		$table->integer('user_id')->unsigned()->nullable();
    		$table->integer('message_id')->unsigned()->nullable();
    		$table->integer('milestone_id')->unsigned()->nullable();
    		$table->integer('todo_list_id')->unsigned()->nullable();
    		$table->integer('file_id')->unsigned()->nullable();
    		$table->nullableTimestamps();
    	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('comments');
    }
}
