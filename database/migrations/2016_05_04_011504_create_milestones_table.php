<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMilestonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::create('milestones', function (Blueprint $table) {
    		$table->increments('id');
    		$table->string('title', 100);
    		$table->integer('user_id')->unsigned()->nullable();
    		$table->dateTime('due_date')->nullable();
    		$table->tinyInteger('status')->nullable();
    		$table->tinyInteger('before_notification_sent')->nullable();
    		$table->tinyInteger('after_notification_sent')->nullable();
    		$table->integer('project_id')->unsigned()->nullable();
    		$table->tinyInteger('complete')->nullable();
    		$table->nullableTimestamps();
    	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('milestones');
    }
}
