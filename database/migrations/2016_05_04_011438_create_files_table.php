<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::create('files', function (Blueprint $table) {
    		$table->increments('id');
    		$table->string('filename', 100);
    		$table->integer('project_id')->unsigned()->nullable();
    		$table->integer('user_id')->unsigned()->nullable();
    		$table->integer('comment_id')->unsigned()->nullable();
    		$table->integer('message_id')->unsigned()->nullable();
    		$table->nullableTimestamps();
    	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('files');
    }
}
