<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::table('files', function(Blueprint $table) {
    		$table->foreign('project_id')->references('id')->on('projects')->onDelete('restrict')->onUpdate('cascade');
    		$table->foreign('user_id')->references('id')->on('users')->onDelete('restrict')->onUpdate('cascade');
    		$table->foreign('comment_id')->references('id')->on('comments')->onDelete('restrict')->onUpdate('cascade');
    		$table->foreign('message_id')->references('id')->on('messages')->onDelete('restrict')->onUpdate('cascade');
    	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::table('files', function(Blueprint $table) {
    		 $table->dropForeign('project_id');
    		 $table->dropForeign('user_id');
    		 $table->dropForeign('comment_id');
    		 $table->dropForeign('message_id');
    	});
    }
}
