<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::create('projects', function (Blueprint $table) {
    		$table->increments('id');
    		$table->string('name', 100);
    		$table->text('description')->nullable();
    		$table->dateTime('end_date')->nullable();
    		$table->string('status', 10)->nullable();
    		$table->nullableTimestamps();
    	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('projects');
    }
}
