<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysToMessagesUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::table('messages_users', function(Blueprint $table) {
    		$table->foreign('message_id')->references('id')->on('messages')->onDelete('restrict')->onUpdate('cascade');
    		$table->foreign('user_id')->references('id')->on('users')->onDelete('restrict')->onUpdate('cascade');
    	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::table('messages_users', function(Blueprint $table) {
    		 $table->dropForeign('message_id');
    		 $table->dropForeign('user_id');
    	});
    }
}
