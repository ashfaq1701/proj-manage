<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysToTodoListStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::table('todo_list_statuses', function(Blueprint $table) {
    		$table->foreign('todo_list_id')->references('id')->on('todo_lists')->onDelete('restrict')->onUpdate('cascade');
    	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::table('todo_list_statuses', function(Blueprint $table) {
    		 $table->dropForeign('todo_list_id');
    	});
    }
}
