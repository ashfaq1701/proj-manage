<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Models\Category;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
    }
}

class UserSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('users')->delete();
		Model::unguard();
		$user = new User();
		$user->name = 'Admin';
		$user->email = 'admin@system.com';
		$user->password = bcrypt('password');
		$user->type = 'admin';
		$user->save();
		Model::reguard();
	}
}

class CategorySeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$categories = ['Development', 'Research', 'Implementation', ]
		Model::unguard();
		$category = 
	}
}
